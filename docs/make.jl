using CacheArrays
using Documenter

DocMeta.setdocmeta!(CacheArrays, :DocTestSetup, :(using CacheArrays); recursive=true)

makedocs(;
    modules=[CacheArrays],
    authors="Expanding Man <savastio@protonmail.com> and contributors",
    repo="https://gitlab.com/ExpandingMan/CacheArrays.jl/blob/{commit}{path}#{line}",
    sitename="CacheArrays.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ExpandingMan.gitlab.io/CacheArrays.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
